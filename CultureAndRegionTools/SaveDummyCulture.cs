﻿namespace CustomCultureTools {

    using System;
    using System.Globalization;
    using System.IO;
    using Infrastructure;

    public static partial class CultureAndRegionTools {

        private const String MustNotBeNullOrWhitespace = "The parameter must not be null or whitespace.";

        public static Int32 SaveDummyCulture(String sourceCultureName,
                                             String sourceRegionName,
                                             String dummyCultureName,
                                             String ldmlFileName) {

            if (String.IsNullOrWhiteSpace(sourceCultureName))
                throw new ArgumentException(MustNotBeNullOrWhitespace, sourceCultureName);

            if (String.IsNullOrWhiteSpace(sourceRegionName))
                throw new ArgumentException(MustNotBeNullOrWhitespace, sourceRegionName);

            if (String.IsNullOrWhiteSpace(dummyCultureName))
                throw new ArgumentException(MustNotBeNullOrWhitespace, dummyCultureName);

            if (String.IsNullOrWhiteSpace(ldmlFileName))
                throw new ArgumentException(MustNotBeNullOrWhitespace, ldmlFileName);

            CultureAndRegionInfoBuilder cib;
            try {
                cib = new CultureAndRegionInfoBuilder(
                    dummyCultureName,
                    CultureAndRegionModifiers.None);
            } catch (ArgumentException ae) {
                Console.Error.WriteLine("** Invalid culture name: {0}", ae.Message);
                return (Int32) ResultCode.INVALID_PARAMETER;
            }

            try {
                cib.LoadDataFromCultureInfo(new CultureInfo(sourceCultureName));
            } catch (CultureNotFoundException ex) {
                Console.Error.WriteLine("** No such culture available to clone: {0}", ex.Message);
                return (Int32) ResultCode.INVALID_PARAMETER;
            }

            try {
                cib.LoadDataFromRegionInfo(new RegionInfo(sourceRegionName));
            } catch (ArgumentException ae) {
                Console.Error.WriteLine("** Invalid region name: {0}", ae.Message);
                return (Int32) ResultCode.INVALID_PARAMETER;
            }

            // Save the new CultureAndRegionInfoBuilder object in the specified file in 
            // LDML format. The file is saved in the same directory as the application  
            // that calls the Save method.

            var fileName = String.Format("{0}.xml", ldmlFileName);

            Console.WriteLine("Saving to {0}...", fileName);
            try {
                cib.Save(fileName);
            } catch (IOException exc) {
                Console.Error.WriteLine("** I/O exception: {0}", exc.Message);
                return (Int32) ResultCode.FILE_EXISTS;
            }

            const String msg2 = "Reconstituting the CultureAndRegionInfoBuilder object from \"{0}\".";
            Console.WriteLine(msg2, fileName);

            var regionAndCultureInfoTester = CultureAndRegionInfoBuilder.CreateFromLdml(fileName);
            PresentCultureInfoBuilder(regionAndCultureInfoTester);

            return (Int32) ResultCode.SUCCESS;
        }

    }

}