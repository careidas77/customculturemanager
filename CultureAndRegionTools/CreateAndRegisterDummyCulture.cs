﻿namespace CustomCultureTools {

    using System;
    using System.Globalization;
    using Infrastructure;

    public static partial class CultureAndRegionTools {

        public static ResultCode CreateAndRegisterDummyCulture(String fourLetterCode = null, String regionCode = null) {
            if (!HasAdministratorPrivileges())
                return ResultCode.ACCESS_DENIED;

            var cultureCode = fourLetterCode ?? "sv-SE";
            var cultureName = String.Format("x-{0}-sample", cultureCode);
            var regionName = regionCode ?? "SE";

            try {
                Console.WriteLine("Create and explore the CultureAndRegionInfoBuilder...\n");
                var cib = new CultureAndRegionInfoBuilder(
                    cultureName,
                    CultureAndRegionModifiers.None);

                var ci = new CultureInfo(cultureCode);
                cib.LoadDataFromCultureInfo(ci);

                var ri = new RegionInfo(regionName);
                cib.LoadDataFromRegionInfo(ri);

                PresentCultureInfoBuilder(cib);

                Console.WriteLine("Register the custom culture...");
                cib.Register();

                Console.WriteLine("Create and explore the custom culture...\n");
                ci = new CultureInfo(cultureName);

                PresentCultureInfo(ci);

            } catch (Exception e) {
                Console.Error.WriteLine("** Fatal error: {0}", e.Message);
                return ResultCode.NOT_SUPPORTED; // Because, why not?
            }

            return ResultCode.SUCCESS;
        }

    }

}