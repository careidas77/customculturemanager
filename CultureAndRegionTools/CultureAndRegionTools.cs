﻿namespace CustomCultureTools {

    using System;
    using System.Globalization;
    using System.Security.Principal;

    // The CultureAndRegionInfoBuilder.Register() method requires administrative priviliges.
    public static partial class CultureAndRegionTools {

        private static void PresentCultureInfoBuilder(CultureAndRegionInfoBuilder cib) {

            if (cib == null)
                throw new ArgumentNullException("cib");

            Console.WriteLine("----------");
            Console.WriteLine("CultureName:. . . . . . . . . . {0}", cib.CultureName);
            Console.WriteLine("CultureEnglishName: . . . . . . {0}", cib.CultureEnglishName);
            Console.WriteLine("CultureNativeName:. . . . . . . {0}", cib.CultureNativeName);
            Console.WriteLine("GeoId:. . . . . . . . . . . . . {0}", cib.GeoId);
            Console.WriteLine("IsMetric: . . . . . . . . . . . {0}", cib.IsMetric);
            Console.WriteLine("ISOCurrencySymbol:. . . . . . . {0}", cib.ISOCurrencySymbol);
            Console.WriteLine("RegionEnglishName:. . . . . . . {0}", cib.RegionEnglishName);
            Console.WriteLine("RegionName: . . . . . . . . . . {0}", cib.RegionName);
            Console.WriteLine("RegionNativeName: . . . . . . . {0}", cib.RegionNativeName);
            Console.WriteLine("ThreeLetterISOLanguageName: . . {0}", cib.ThreeLetterISOLanguageName);
            Console.WriteLine("ThreeLetterISORegionName: . . . {0}", cib.ThreeLetterISORegionName);
            Console.WriteLine("ThreeLetterWindowsLanguageName: {0}", cib.ThreeLetterWindowsLanguageName);
            Console.WriteLine("ThreeLetterWindowsRegionName: . {0}", cib.ThreeLetterWindowsRegionName);
            Console.WriteLine("TwoLetterISOLanguageName: . . . {0}", cib.TwoLetterISOLanguageName);
            Console.WriteLine("TwoLetterISORegionName: . . . . {0}", cib.TwoLetterISORegionName);
            Console.WriteLine();
        }

        private static void PresentCultureInfo(CultureInfo ci) {

            if (ci == null)
                throw new ArgumentNullException("ci");

            Console.WriteLine("----------");
            Console.WriteLine("Name: . . . . . . . . . . . . . {0}", ci.Name);
            Console.WriteLine("EnglishName:. . . . . . . . . . {0}", ci.EnglishName);
            Console.WriteLine("NativeName: . . . . . . . . . . {0}", ci.NativeName);
            Console.WriteLine("TwoLetterISOLanguageName: . . . {0}", ci.TwoLetterISOLanguageName);
            Console.WriteLine("ThreeLetterISOLanguageName: . . {0}", ci.ThreeLetterISOLanguageName);
            Console.WriteLine("ThreeLetterWindowsLanguageName: {0}", ci.ThreeLetterWindowsLanguageName);
            Console.WriteLine();
        }

        // Microsoft recommends the following practice for UAC compatibility rather than setting the
        // <requestedExecutionLevel  level="requireAdministrator" uiAccess="false" />
        // in the app.manifest, see https://msdn.microsoft.com/en-us/library/bb756922.aspx.
        private static Boolean HasAdministratorPrivileges() {
            var id = WindowsIdentity.GetCurrent();

            if (id == null)
                throw new IdentityNotMappedException("Unable to determine the user identity.");

            var principal = new WindowsPrincipal(id);
            return principal.IsInRole(WindowsBuiltInRole.Administrator);
        }

    }

}