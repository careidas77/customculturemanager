﻿namespace CustomCultureTools {

    using System;
    using System.Globalization;
    using Infrastructure;

    public static partial class CultureAndRegionTools {

        public static Int32 UnregisterCulture(String cultureName) {
            if (String.IsNullOrWhiteSpace(cultureName))
                throw new ArgumentException("The culture name must not be null or whitespace.", "cultureName");

            try {
                CultureAndRegionInfoBuilder.Unregister(cultureName);
            } catch (Exception e) {
                Console.Error.WriteLine("** Error while unregistering: {0}", e.Message);
                return (Int32) ResultCode.NOT_SUPPORTED;
            }

            Console.WriteLine("Culture {0} successfully unregistered.", cultureName);
            return (Int32) ResultCode.SUCCESS;
        }

    }

}