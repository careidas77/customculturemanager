﻿namespace CustomCultureTools.Infrastructure {

    public enum ResultCode {

        SUCCESS = 0,
        FILE_NOT_FOUND = 2,
        PATH_NOT_FOUND = 3,
        ACCESS_DENIED = 5,
        NOT_SUPPORTED = 50,
        FILE_EXISTS = 80,
        INVALID_PARAMETER = 87,
        XML_PARSE_ERROR = 1465,
        INVALID_COMMAND_LINE = 0x667

    }

}