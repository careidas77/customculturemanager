﻿namespace CustomCultureTools {

    using System;
    using System.Globalization;
    using System.IO;
    using System.Xml;
    using Infrastructure;

    public static partial class CultureAndRegionTools {

        public static Int32 RegisterCulture(String ldmlfileName) {
            if (String.IsNullOrWhiteSpace(ldmlfileName))
                throw new ArgumentException("The parameter must not be null or whitespace.", ldmlfileName);

            Console.WriteLine("Parsing LDML...");
            CultureAndRegionInfoBuilder cib;
            try {
                cib = CultureAndRegionInfoBuilder.CreateFromLdml(ldmlfileName);
            } catch (ArgumentNullException xe) {
                Console.Error.WriteLine("** The LDML file path must not be null: {0}", xe.Message);
                return (Int32) ResultCode.INVALID_PARAMETER;
            } catch (FileNotFoundException xe) {
                Console.Error.WriteLine("** File not found: {0}", xe.Message);
                return (Int32) ResultCode.FILE_NOT_FOUND;
            } catch (DirectoryNotFoundException xe) {
                Console.Error.WriteLine("** Directory not found: {0}", xe.Message);
                return (Int32) ResultCode.PATH_NOT_FOUND;
            } catch (XmlException xe) {
                Console.Error.WriteLine("** XML validation exception: {0}", xe.Message);
                return (Int32) ResultCode.XML_PARSE_ERROR;
            }

            PresentCultureInfoBuilder(cib);

            return RegisterCulture(cib);
        }

        public static Int32 RegisterCulture(CultureAndRegionInfoBuilder cib) {
            if (cib == null)
                throw new ArgumentNullException("cib");

            if (!HasAdministratorPrivileges()) {
                Console.Error.WriteLine("Access denied - you need to run the console as Administrator.");
                return (Int32) ResultCode.ACCESS_DENIED;
            }

            try {
                Console.WriteLine("Register the custom culture...");
                cib.Register();
            } catch (Exception e) {
                Console.Error.WriteLine("** Fatal error: {0}", e.Message);
                return (Int32) ResultCode.NOT_SUPPORTED; // Because, why not?
            }

            Console.WriteLine("Reading culture...");
            var ci = new CultureInfo(cib.CultureName);
            PresentCultureInfo(ci);

            Console.WriteLine("Seems OK.");
            return (Int32) ResultCode.SUCCESS;
        }

    }

}