Simple console application to manage cloning, registering and unregistering cultures and custom cultures on Windows servers using the CultureAndRegionInfoBuilder class.

The solution contains two projects. The console application is rudimentary, but will allow managing custom cultures through a console window using the following commands:

* REGISTER filename
* UNREGISTER -culture cultureName
* SAVEDUMMY [-culture sourceCultureName] [-region sourceRegionName] [-dummy dummyName] [fileName]

For the REGISTER command, point to a LDML culture file to register.
The SAVEDUMMY command will generate a LDML culture file. It defaults to -culture sv-SE and -region SE, and saving the LDML in the current directory as x-sv-SE-dummy.xml. Use the optional parameters to customize this behavior.

The second project is a simple class library with no external dependencies. This you can use if you want to incorporate the ability to manage cultures in your own application. Available methods are:

CreateAndRegisterDummyCulture - it's there, but I didn't really intend for it to be used...
RegisterCulture - using a LDML file like above, or your very own CultureAndRegionInfoBuilder with the overload.
SaveDummyCulture - creates an LDML file as described above.
UnregisterCulture - unregisters a culture from the server (obviously, use with caution).

NB: Registering a culture on a server requires administrative privileges.

### Contribution guidelines ###

For all contributions I kindly ask that you take the time to write sufficient comments that I will be able to understand why you made the change or addition in question ;)

### Who do I talk to? ###
grebdnil.rakso@gmail.com