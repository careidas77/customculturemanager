﻿using CustomCultureTools.Infrastructure;

namespace CustomCultureManager {

    using System;
    using System.Linq;
    using System.Text.RegularExpressions;
    using CustomCultureTools;

    internal class Program {

        private static Int32 Main(String[] args) {
            if (args == null || args.Length == 0) {
                Console.WriteLine("Supported commands are:");
                foreach (var name in Enum.GetNames(typeof (SupportedCommands))) {
                    Console.WriteLine(name);
                }

                return (Int32) ResultCode.INVALID_COMMAND_LINE;
            }

            var commandName = args.First();

            var flags = Regex.Matches(String.Join(" ", args), @"(?<flag>-[^\s]*)\s(?<value>[^\s]*)")
                             .Cast<Match>()
                             .ToDictionary(m => m.Groups["flag"].Value, m => m.Groups["value"].Value);

            var fileName =
                args.LastOrDefault(
                    a => a != null && a != commandName && !flags.ContainsKey(a) && !flags.ContainsValue(a));

            String cultureName;
            flags.TryGetValue("-culture", out cultureName);

            SupportedCommands command;
            Enum.TryParse(commandName, true, out command);

            Console.Clear();

            switch (command) {
                case SupportedCommands.REGISTER:
                    if (fileName == null) {
                        Console.Error.WriteLine(
                            "You need to supply a file name as the last parameter.");
                        return (Int32) ResultCode.INVALID_PARAMETER;
                    }

                    return CultureAndRegionTools.RegisterCulture(fileName);

                case SupportedCommands.UNREGISTER:
                    if (String.IsNullOrWhiteSpace(cultureName)) {
                        Console.Error.WriteLine(
                            "You need to supply -culture 'culture name'.");
                        return (Int32) ResultCode.INVALID_PARAMETER;
                    }
                    return CultureAndRegionTools.UnregisterCulture(cultureName);

                case SupportedCommands.SAVEDUMMY:
                    const String defaultCulture = "sv-SE";
                    const String defaultRegion = "SE";

                    if (String.IsNullOrWhiteSpace(cultureName))
                        cultureName = defaultCulture;

                    String regionName;
                    if (!flags.TryGetValue("-region", out regionName))
                        regionName = defaultRegion;

                    String dummyCultureName;
                    if (!flags.TryGetValue("-dummy", out dummyCultureName))
                        dummyCultureName = String.Format("x-{0}-dummy", cultureName);

                    return
                        CultureAndRegionTools.SaveDummyCulture(
                            cultureName,
                            regionName,
                            dummyCultureName,
                            fileName ?? dummyCultureName);

                default:
                    Console.Error.WriteLine("Unsupported command.");
                    return (Int32) ResultCode.NOT_SUPPORTED;
            }
        }

    }

    internal enum SupportedCommands {

        REGISTER,
        UNREGISTER,
        SAVEDUMMY

    }

}